<?php
/**
 * @file
 * Provides a visitor context plugin for Sharpspring user profiles

 */

class SharpspringContext extends PersonalizeContextBase {

  /**
   * A fully-loaded user object representing the current user.
   *
   * @var stcClass
   */
  protected $sharpSpringAPI;

  /**
   * Implements PersonalizeContextInterface::create().
   */
  public static function create(PersonalizeAgentInterface $agent = NULL, $selected_context = array()) {
    try {
      //@todo try to get context from sharpspring.
      $sharpspring = new stdClass();
      return new self($agent, $selected_context, $sharpspring);
    }
    catch (Exception $e) {
      return NULL;
    }
  }

  /**
   * Override the parent constructor to add the user account.
   */
  public function __construct(PersonalizeAgentInterface $agent = NULL, $selected_context, $sharpspring) {
    parent::__construct($agent, $selected_context);
    $this->sharpspringAPI = $sharpspring;
  }

  /**
   * Implements PersonalizeContextInterface::getOptions().
   */
  public static function getOptions() {
    // Get options for available variables.
    $options['test'] = array(
      'name' => 'test',
      'group' => 'Sharpspring',
      'cache_type' => 'session'
    );
    $options['Email'] = array(
      'name' => 'Email',
      'group' => 'Sharpspring',
      'cache_type' => 'session'
    );

    return $options;
  }

  /**
   * Implements PersonalizeContextInterface::getAssets().
   */
  public function getAssets() {
    return array(
      'js' => array(
          drupal_get_path('module', 'sharpspring_personalize') . '/js/personalize_sharpspring_context.js' => array(),
      ),
    );
  }

  /**
   * Implements PersonalizeContextInterface::getPossibleValues().
   */
  public function getPossibleValues($limit = FALSE) {

    $data = array(
      'method' => 'getFields',
      'params' => array('where' => array()),
      'id' => session_id(),
    );

    $account_id = variable_get('sharpspring_api_account_id');
    $secret_key = variable_get('sharpspring_api_secret_key');
    $result = '';
    $return_defaults = FALSE;

    if(!$account_id || !$secret_key) {
      drupal_set_message('For advanced sharpspring integrations, set your Account ID and Secret Key on the !url', array('!url' => l('SharpSpring Settings Page', 'admin/config/system/sharpspring/settings')));
      $return_defaults = TRUE;
    } else {
      $account_data = array(
        'accountID' => $account_id,
        'secretKey' => $secret_key
      );

      $url = url('http://api.sharpspring.com/pubapi/v1/', array('query' => array($account_data)));


      $data = json_encode($data);
      $ch = curl_init($url);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
      curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($data)
      ));

      $result = json_decode(curl_exec($ch));
      curl_close($ch);

      $possible_values = array();

      if ($result) {
        if (!empty($result->result->field)) {
          foreach ($result->result->field as $field_data) {
            if (!$field_data->isActive) {
              continue;
            }
            $possible_values[$field_data->label]['friendly name'] = $field_data->label . ' (SharpSpring)';
            // @todo: add custom handling for date
            switch ($field_data->dataType) {
              case 'int':
              case 'double':
                $possible_values[$field_data->label]['value type'] = 'number';
                break;
              default;
                $possible_values[$field_data->label]['value type'] = 'string';

            }
          }
        } else {
          $return_defaults = TRUE;
        }
      }

    }
    if($return_defaults) {
      $possible_values = array(
        'Company Name' => array(
          'friendly name' => 'Company Name (SharpSpring)',
          'value type' => 'string',
          ),
        'Email' => array(
          'friendly name' => 'Email (SharpSpring)',
          'value type' => 'string',
        ),
        'First Name' => array(
          'friendly name' => 'First Name',
          'value type' => 'string',
        ),
        'Is Contact' => array(
          'friendly name' => 'Is Contact (SharpSpring)',
          'value type' => 'number',
        ),
        'Last Name' => array(
          'friendly name' => 'Last Name (SharpSpring)',
          'value type' => 'string',
        ),
        'Lead Score' => array(
          'friendly name' => 'Lead Score (SharpSpring)',
          'value type' => 'number',
        ),
        'Lead Status' => array(
          'friendly name' => 'Lead Status (SharpSpring)',
          'value type' => 'string',
        ),
        'SharpSpring ID' => array(
          'friendly name' => 'SharpSpring ID (SharpSpring)',
          'value type' => 'number',
        ),
        'Website' => array(
          'friendly name' => 'Website (SharpSpring)',
          'value type' => 'string',
        ),
      );
    }

    if ($limit) {
      $possible_values = array_intersect_key($possible_values, array_flip($this->selectedContext));
    }
    $possible_values['Has Record']  = array(
      'friendly name' => 'Has Record (SharpSpring)',
      'value type' => 'predefined',
      'values' => array(1 => 'Yes', 0 => 'No')
    );
    return $possible_values;
  }
}
