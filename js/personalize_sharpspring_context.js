(function ($) {
  /**
   * Visitor Context object.
   *
   * This object holds the context for the active user which will be used
   * as the basis of personalization. Agents may add additional information
   * to this object as they work with it, or other code may place context
   * within this object which can be used later by agents or used
   * on subsequent page loads.
   */
  Drupal.personalize = Drupal.personalize || {};
  Drupal.personalize.visitor_context = Drupal.personalize.visitor_context || {};
  Drupal.settings.sharpspring =  Drupal.settings.sharpspring || {};
  Drupal.settings.sharpspring.calledFuncs =  Drupal.settings.sharpspring.calledFuncs || [];
  /**
   * The User Profile context plugin.
   *
   *   be added when the plugin is being used.
   */
  Drupal.personalize.visitor_context.sharpspring_context = {
    'getContext': function(enabled) {

      var  context_values = {};

      return new Promise(function(resolve, reject){
        // Define a callback function to receive information about the segments
        // for the current visitor.
        var segmentsCallback = function (resp) {
          context_values = resp;
          if(resp.hasOwnProperty('SharpSpring ID')) {
            context_values['Has Record'] = 1;
          } else {
            context_values['Has Record'] = 0;
          }
          resolve(context_values);
        };
        Drupal.settings.sharpspring.calledFuncs = Drupal.settings.sharpspring.calledFuncs || [];
        // Register our callback for receiving segments.
        Drupal.settings.sharpspring.calledFuncs.push(segmentsCallback);
        callbackRegistered = true;
      });
    }
  };

})(jQuery);

